# V-CLI-deo

**V-CLI-deo** ist ein CLI-Skript, mit welchen man Videos von YouTube und anderen Plattformen mit dem freien und Quelloffenen Programm <a href="https://github.com/yt-dlp/yt-dlp"><strong>yt-dlp</strong></a> herunterladen kann.

##### V-CLI-deo im Linux Terminal (XFCE)

![V-CLI-deo im Linux Terminal (XFCE)](img/V-CLI-deo%20Linux.png)

##### V-CLI-deo im Windows CMD

![V-CLI-deo im Windows CMD](img/V-CLI-deo%20Windows.png)

## Installation

Da **V-CLI-deo** ein Skirpt ist, installiert man dieses manuell.

### Linux:

Im Terminal gibt man folgendes ein:

```bash
git clone https://codeberg.org/Monstanner/V-CLI-deo.git && cd V-CLI-deo && rm LICENSE README.md && rm -r Windows && mv Linux/* ./ && rm -r img && rmdir Linux && chmod u+xwr -R .git && rm -r .git
```
Nun installiert man diese benötigten Pakete mit dem Paket-Manager der verwendeten Linux Distribution:

> `yt-dlp`, `ffmpeg`, `ffprobe`

Auf Debian-basierten Distributionen gibt man dies im Terminal ein:

> `sudo apt install yt-dlp ffmpeg ffprobe`

Auf Arch-basierten Distributionen gibt man dies im Terminal ein:

> `sudo pacman -S yt-dlp ffmpeg ffprobe`

Abschließende Schritte:

* Im Dateimanager `V-CLI-deo.sh` und `V-CLI-deo.desktop` ausführbar machen.<br><br>
Anderfalls ein Terminal im Ordner **V-CLI-deo** öffnen und dies eingeben > `chmod u+x V-CLI-deo.desktop V-CLI-deo.sh`
* Die Pfade in V-CLI-deo.desktop ändern (Exec und Icon) zu Deinen Pfaden.<br>
(Beispiel: `/home/deinpfad/V-CLI-deo` )
* Verschiebe `V-CLI-deo.desktop` nach `/home/user/.local/share/applications`.
Anderfalls ein Terminal im Ordner **V-CLI-deo** öffnen und dies eingeben > `mv V-CLI-deo.desktop $HOME/.local/share/applications`

### Windows

Als erstes lädt man diese benötigten Pakete herunter:

 <a href="https://codeberg.org/Monstanner/V-CLI-deo"><strong>V-CLI-deo</strong></a>, <a href="https://github.com/yt-dlp/yt-dlp/releases"><strong>yt-dlp</strong></a>, <a href="https://www.gyan.dev/ffmpeg/builds/#release-builds"><strong>ffmpeg</strong></a>

 Folgende Pakete werden auf den Paketseiten heruntergeladen:

 #### V-CLI-deo:

 Hier lädt man das Repository wahlweise als Zip oder als TAR.GZ herunter.

 #### yt-dlp:

`yt-dlp_win.zip`

 #### ffmpeg:

 `ffmpeg-release-full.7z`

Ist alles heruntergeladen, wird nun alles in seperaten Ordnen entpackt.

*Falls man das ffmpeg-Paket oder ein anderes Paket nicht entpacken kann, sollte <a href="https://www.7-zip.org/"><strong>7z</strong></a> weiterhelfen.*

Nachdem entpacken, gibt es nun drei Ordner. `V-CLI-deo`, `ffmpeg` & `yt-dlp`.

Nun fügt man folgende Datein zusammen. Deswegen sollte ein primärer Ordnern erstellt werden. Zum Beispiel unter `C:\Users\DeinName\yt-dlp`.

Folgende Datein werden in den primären Ordner verschoben:

V-CLI-deo\Windows:

> `V-CLI-deo.bat`, `V-CLI-deo.ico`

ffmpeg\bin:

> `ffmpeg.exe`, `ffplay.exe`, `ffprobe.exe`

yt-dlp\:

> `den gesamten Inhalt.`

Abschließende Schritte:

* Mit einen beliebigen Texteditor (Notepad, Notepad++, etc.) sucht man mit STRG+F nach `#Bitte das README.md lesen.` und ersetzt diese Zeilen mit `C:/Users/DeinName/Videos/` und speichert das Skript mit STRG+S ab. (Man kann den Pfad auch individuell gestallten. Beispiele: `D:/Videos/Downloads`, `C:/Users/DeinName/Downloads/Videos`, etc.)
* Dann kann man von den Skript eine Verknüpfung auf den Desktop erstellen und das beigelegete Icon als Verknüfungs-Icon auswählen.

## Probleme

Da Google LLC. stetig Änderungen an YouTube vornimmt, kann es vorkommen, dass das Skript hin und wieder streikt. Das sollte mit einer einfachen und stetigen Aktualisierung von `yt-dlp` zu lösen sein:

### Linux:
```bash
yt-dlp -U
```

### Windows:

V-CLI-deo öffnen, 5 eingeben, enter drücken, J eingeben, enter drücken, warten, wenn der Text erscheint, dass man eine beliebige Taste zum beenden drücken soll, macht man dies.

## Wie man V-CLI-deo nutzt

### Linux

V-CLI-deo sollte jetzt im Startmenü / Menü / Dash / etc. der jeweiligen Distribution aufzufinden sein. Ein klick auf das Symbol öffnet das standart Terminal. Ganz oben sieht man den Namen des Skripts und die Versionsnummer und den Autor.
Darauf folgen `Ziffern` mit Plattformen dahinter. Und die Frage, von welcher Plattform das Video heruntergeladen werden soll.

Hat man sich für eine Plattform entschieden, gibt man diese `Ziffer` ein und bestätigt dies dann mit `Enter`. Darauf folgt entweder die bitte nach den Link des Videos, welches heruntergeladen werden soll (ÖRR & Andere Plattform), oder eine weitere folge mit Ziffern mit Auflösungen dahinter und die Frage, in welcher Auflösung das Video heruntergeladen werden soll, mit einer anschließende bitte nach den Link des Videos (YouTube). Diesen fügt man mit `STRG / CTRL + Shift + V` oder `Rechtsklick + Einfügen` ein und bestätigt das ganze wieder mit `Enter`. Dann wird das gewünschte Video heruntergeladen (YouTube & Andere Plattform). Beim ÖRR wird es etwas anspruchsvoller. Nachdem man den Link eingefügt und mit `Enter` bestätigt hat, erscheint eine Liste mit sehr vielen Informationen. Hier ist die `ID` / sind die `ID's`  des Videos wichtig. Die `ID` / `ID's` gibt / geben die Auflösung & Audio an. In der Mediathek der ARD hat man kaum Inhalte mit zwei `ID's`, anders sieht es bei der Mediathek vom ZDF oder von Arte aus. Dort gibt es meist mehrere Sprachen. Hier entscheidet man , welche Qualität und oder Sprache das Video haben soll.

ÖRR Beispiel:

![Beispiel von ID's im Linux Terminal (XFCE)](img/%C3%96RR%20ID%27s%20Linux.png)


Im Beispiel sieht man, dass die *Vidoe-ID* `hls-5367-5` eine auf Auflösung von 1920x1080 und 50 fps hat. Als *Audio-ID* `http-h265_aac_mp4_http_na_na-uhd-0` wurde deutsch mit der zweit höchsten Qualität ausgewählt. Dann wurde das ganze mit `Enter` bestätigt und das herunterladen hat begonnen.

Wenn dies fertiggestellt ist, beendet sich das Skirpt automatisch. Viel Spaß beim schauen von Videos / Filmen / etc.

### Windows

Hat man eine Verknüpfung von V-CLI-deo erstellt, doppelklickt man auf das Symbol, so öffnet sich das Windows CMD. Ganz oben sieht man den Namen des Skripts und die Versionsnummer und den Autor. Darauf folgen `Ziffern` und `Buchstaben` mit Plattformen oder Optionen dahinter. Und die Frage, von welcher Plattform das Video heruntergeladen werden soll.

Hat man sich für eine Plattform entschieden, gibt man diese `Ziffer` oder den `Buchstaben` ein und bestätigt dies dann mit `Enter`. Darauf folgt entweder die bitte nach den Link des Videos, welches heruntergeladen werden soll (ÖRR & Andere Plattform), oder eine weitere folge mit Ziffern mit Auflösungen dahinter und die Frage, in welcher Auflösung das Video heruntergeladen werden soll, mit einer anschließende bitte nach den Link des Videos (YouTube). Diesen fügt man mit `STRG / CTRL + Shift + V` oder `Rechtsklick` ein und bestätigt das ganze wieder mit `Enter`. Dann wird das gewünschte Video heruntergeladen (YouTube & Andere Plattform). Beim ÖRR wird es etwas anspruchsvoller. Nachdem man den Link eingefügt und mit `Enter` bestätigt hat, erscheint eine Liste mit sehr vielen Informationen. Hier ist die `ID` / sind die `ID's`  des Videos wichtig. Die `ID` / `ID's` gibt / geben die Auflösung & Audio an. In der Mediathek der ARD hat man kaum Inhalte mit zwei `ID's`, anders sieht es bei der Mediathek vom ZDF oder von Arte aus. Dort gibt es meist mehrere Sprachen. Hier entscheidet man, in welcher Qualität und oder Sprache das Video haben soll.

Beispiel:

![Beispiel von ID's im Windows CMD](img/%C3%96RR%20ID%27s%20Windows.png)


Im Beispiel sieht man, dass die *Video-ID* `hls-5367-5` eine auf Auflösung von 1920x1080 und 50 fps hat. Als *Audio-ID* `http-h265_aac_mp4_http_na_na-uhd-0` wurde deutsch mit der zweit höchsten Qualität ausgewählt. Dann wurde das ganze mit `Enter` bestätigt und das herunterladen hat begonnen.

Wenn dies fertiggestellt ist, steht im CMD, dass man eine beliebige Taste zum beenden drücken soll. Dies macht man oder schließt es mit dem X oben Rechts. Viel Spaß beim schauen von Videos / Filmen / etc.

## FAQ

Falls noch Fragen zu dem Skirpt vorhanden sind, hoffe ich sie hier beantworten zu können.

F: Was bedeutet CLI?<br>
A: CLI heißt Command-Line-Interface. Und ist die Konsole / das Terminal unter Linux und bei Windows ist es CMD. Also verwendet das Skript keine GUI (Graphical-User-Interfce/Grafische-Benutzer-Oberfläche).

F: Was heißt V-CLI-deo und wie spricht man es aus?<br>
A: Was CLI bedeutet weißt Du sicher jetzt. Doch wie soll man V-CLI-deo ausprechen? Etwa so: V Pause C L I Pause deo? Nein. Es ist einfacher. Video! Das CLI wurde als Indikator eingesetzt, da es "Video" auch mit einer GUI gibt und so heißt diese Variante <a href="https://codeberg.org/Monstanner/V-GUI-deo"><strong>V-GUI-deo</strong></a>.

F: Darf ich das Skript forken?<br>
A: Ja, denn es ist Quelloffen und untersteht der <a href="https://codeberg.org/Monstanner/V-CLI-deo/src/branch/main/LICENSE"><strong>GPL 3.0</strong></a>. Eine verlinkung zu diesen Skript wäre nett.

## Zukünftige Versionen Release Notes / To-Do's

###### V-CLI-deo v.1.X XXXXXXXX

* [ ] Unterstützung für Videos mit mehrsprachigen Audiospuren im ÖRR-Download.

## Release Notes
**Anmerkung:** *KRelease = Kein Release*

###### V-CLI-deo v.1.3.1 20241126

* Falsche Qualitäts-ID behoben und oder entfernt. (Linux & Windows)
* Unterstützung für YouTube Premium wieder entfernt. (Linux & Windows)
* README.md aktualisiert.

###### V-CLI-deo v.1.3 20230807

* Release der Linux und Windows Version als Zip Pakete.
* Unterstützung für YouTube Premium hinzugefügt. (Linux & Windows)
* Fehlende Qualität für den Buchstaben O / o hinzugefügt. (Windows)
* ReadME.txt aus dem Linux und Windows Verzeichnis entfernt.
* Download-Befehl für Linux in ReadME.md überarbeitet. (&& rm ReadMe.txt entfernt & rm -r img hinzugefügt)


###### KRelease 20230618
* Bild (V-CLI-deo Windows.png) in main/img gelöscht, da es einen Fehler enthielt.
* Bild (V-CLI-deo Windows.png) in main/img hochgeladen (korrigierte Version).
* README.md überarbeitet.

##### KRelease 20230601

* Bilder (V-CLI-deo Windows.png & ÖRR ID's Windows.png) in main/img hochgeladen.
* Bild (ÖRR ID's Windows.png) in main/img gelöscht, da es einen Fehler enthielt.
* Bild (ÖRR ID's Windows.png) in main/img hochgeladen (korrigierte Version).
* README.md überarbeitet und aktualisiert.

##### V-CLI-deo v.1.2 20230301

* Das Skript verwendet jetzt yt-dlp anstatt youtube-dl. (Linux & Windows)
* Die Prüfung ob youtube-dl installiert ist, wurde zu yt-dlp geändert. (Linux)
* Installation von yt-dlp (youtube-dl) nach fehlgeschlagener Prüfung wurde entfernt. (Linux)
* Hinweis entfernt, welcher besagte, dass youtube-dl Probleme bei der Herunterladegeschwindigkeit hat. (Linux)
* Eintrag zum Akatualisieren von yt-dlp hinzugefügt. (Windows)
* Texterklärung was man eingeben muss, wurde ergänzt. (Linux & Windows)
* Neue Plattformen implementiert. (Linux & Windows)
* Neue Baum-Struktur (YouTube, ÖRR, Andere Plattform). (Linux & Windows)
* Die GitHub Links wurden zu Codeberg.org geändert. (Linux & Windows)
* Die Windows Version 1.1 wurde übersprungen, um mit der Linux Version gleichauf zu sein. (Windows)
* Neues main-Verzeichnis (img).
* Bilder von V-CLI-deo in main/img hochgeladen.
* README.md überarbeitet und aktualisiert.

##### KRelease 20220911

* Umzug zu codeberg.org

##### V-CLI-deo v.1.1 20211106 (Linux)

* IF-Abfrage integriert, welche prüft ob youtube-dl installiert ist. Falls es installiert ist, macht V-CLI-deo wie gewohnt weiter. Ist youtube-dl hingegen nicht installiert, wird dies nachgeholt. (<a href="https://github.com/Monstanner/V-CLI-deo/commit/add412a8104f3c97ba5684e05519cd081f9be77e"><strong>GitHub</strong></a>)
* Downloadverzeichnis gestzt (DOWNDIR). (<a href="https://github.com/Monstanner/V-CLI-deo/commit/add412a8104f3c97ba5684e05519cd081f9be77e"><strong>GitHub</strong></a>)
* Rechtschreibfehler in Aud-CLI-o.desktop verbessert. (<a href="https://github.com/Monstanner/V-CLI-deo/commit/13f449f28dfe9145c1707242ca9d3fb1bd3bcd6f"><strong>GitHub</strong></a>)


###### V-CLI-deo v.1.0 20211103 (Windows)

* Erste Veröffentlichung der Windows Version. (<a href="https://github.com/Monstanner/V-CLI-deo/commit/921be164662b99f0c68e15ce564918429051d414"><strong>GitHub</strong></a>)
* Rechtschreibfehler in der Linux ReadMe.txt verbessert. (<a href="https://github.com/Monstanner/V-CLI-deo/commit/d33b70fee5cd59e349c0bf5b3fcb262e277d7a81"><strong>GitHub</strong></a>)


###### V-CLI-deo v.1.0 20211009 (Linux)

* Erste Veröffentlichung der Linux Version. (<a href="https://github.com/Monstanner/V-CLI-deo/commit/cc9dfa4af4ebf46cf2c0b1eac1b12235d0bc3578"><strong>GitHub</strong></a>)
* Schreibfehler behoben (Vorher V-CLI-deo v.1.1 20211009 by Monstanner. Nachher V-CLI-deo v.1.0 20211009 by Monstanner) (<a href="https://github.com/Monstanner/V-CLI-deo/commit/9dbd03a77b1b7e43fb66168b37ddd43e985425e3"><strong>GitHub</strong></a>)

## Lizenz

Autor: Marcel (Monstanner) S.

Lizenz: GPL 3.0

Repository: https://codeberg.org/Monstanner/V-CLI-deo

## Kontakt

<a href="https://mastodon.social/@monstanner"><strong>Mastodon</strong></a> <br>
<a href="mailto:monstanner@gmail.com"><strong>E-Mail</strong></a>
