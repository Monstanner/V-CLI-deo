#!/bin/bash

if which yt-dlp >/dev/null;
then echo "yt-dlp ist installiert." &>/dev/null
else echo "yt-dlp ist nicht installiert. Um V-CLI-deo verwenden zu können, muss yt-dlp installiert werden."
fi


#!/bin/bash

echo ---------------------------------------------------
echo V-CLI-deo v.1.3.1 20241126 by Monstanner
echo ---------------------------------------------------

DOWNDIR=$HOME/Videos/Heruntergeladen/
mkdir $DOWNDIR 2> /dev/null

#!/bin/bash

PS3='Von welcher Plattform soll heruntergeladen werden (Zahl eingeben > Enter)? '
options=("YouTube (Videoplattform von Google LLC)" "ÖRR (Öffentlich Rechtlicher Rundfunk (3sat, ARD, Arte, BR, Funk, ZDF, uvw.))" "Andere Plattform" "Über" "Beenden")
select opt in "${options[@]}"
do
  case $opt in
          "YouTube (Videoplattform von Google LLC)")
              PS3='In welcher Qualitaet soll das Video heruntergeladen werden (Zahl eingeben > Enter). Das Video sollte in der Qualität verfuegbar sein, anderfalls, wird nichts heruntergeladen. '
		      options=("144p (Low-Definition)" "240p (Low-Definition)" "360p (Low-Definition)" "480p (Standard-Definition)" "720p (High-Definition)" "720p60fps (High-Definition + 60fps)" "1080p (Full-High-Definition)" "1080p60fps (Full-High-Definition + 60fps)" "1440p (Quad-High-Definition)" "1440p60fps (Quad-High-Definition + 60fps)" "2160p (Ultra-High-Definition 4K)" "2160p60fps (Ultra-High-Definition 4K + 60fps)" "Beenden")
              select opt in "${options[@]}"
        do
               case $opt in
                  "144p (Low-Definition)")
                   echo ---------------------------------------------------
                   echo "Bitte füge den Link zu dem Video ein (Strg / Ctrl + Shift + V oder Rechtsklick + Einfügen > Enter)."
                   echo ---------------------------------------------------
                   while read INPUT
                   do
                    echo Wird heruntergeladen...
                    yt-dlp -f 160+bestaudio --merge-output-format mp4 -o "$DOWNDIR%(title)s.%(ext)s" "$INPUT"
                    break
                   done
                   exit
                   ;;
                  "240p (Low-Definition)")
                   echo ---------------------------------------------------
                   echo "Bitte füge den Link zu dem Video ein (Strg / Ctrl + Shift + V oder Rechtsklick + Einfügen > Enter)."
                   echo ---------------------------------------------------
                   while read INPUT
                   do
                    echo Wird heruntergeladen...
                    yt-dlp -f 133+bestaudio --merge-output-format mp4 -o "$DOWNDIR%(title)s.%(ext)s" "$INPUT"
                    break
                   done
                   exit
                   ;;
                  "360p (Low-Definition)")
                   echo ---------------------------------------------------
                   echo "Bitte füge den Link zu dem Video ein (Strg / Ctrl + Shift + V oder Rechtsklick + Einfügen > Enter)."
                   echo ---------------------------------------------------
                   while read INPUT
                   do
                    echo Wird heruntergeladen...
                    yt-dlp -f 134+bestaudio --merge-output-format mp4 -o "$DOWNDIR%(title)s.%(ext)s" "$INPUT"
                    break
                   done
                   exit
                   ;;
                  "480p (Standard-Definition)")
                   echo ---------------------------------------------------
                   echo "Bitte füge den Link zu dem Video ein (Strg / Ctrl + Shift + V oder Rechtsklick + Einfügen > Enter)."
                   echo ---------------------------------------------------
                   while read INPUT
                   do
                    echo Wird heruntergeladen...
                    yt-dlp -f 135+bestaudio --merge-output-format mp4 -o "$DOWNDIR%(title)s.%(ext)s" "$INPUT"
                    break
                   done
                   exit
                   ;;
                  "720p (High-Definition)")
                   echo ---------------------------------------------------
                   echo "Bitte füge den Link zu dem Video ein (Strg / Ctrl + Shift + V oder Rechtsklick + Einfügen > Enter)."
                   echo ---------------------------------------------------
                   while read INPUT
                   do
                    echo Wird heruntergeladen...
                    yt-dlp -f 136+bestaudio --merge-output-format mp4 -o "$DOWNDIR%(title)s.%(ext)s" "$INPUT"
                    break
                   done
                   exit
                   ;;
                  "720p60fps (High-Definition + 60fps)")
                   echo ---------------------------------------------------
                   echo "Bitte füge den Link zu dem Video ein (Strg / Ctrl + Shift + V oder Rechtsklick + Einfügen > Enter)."
                   echo ---------------------------------------------------
                   while read INPUT
                   do
                    echo Wird heruntergeladen...
                    yt-dlp -f 136+bestaudio --merge-output-format mp4 -o "$DOWNDIR%(title)s.%(ext)s" "$INPUT"
                    break
                   done
                   exit
                   ;;
                  "1080p (Full-High-Definition)")
                   echo ---------------------------------------------------
                   echo "Bitte füge den Link zu dem Video ein (Strg / Ctrl + Shift + V oder Rechtsklick + Einfügen > Enter)."
                   echo ---------------------------------------------------
                   while read INPUT
                   do
                    echo Wird heruntergeladen...
                    yt-dlp -f 137+bestaudio --merge-output-format mp4 -o "$DOWNDIR%(title)s.%(ext)s" "$INPUT"
                    break
                   done
                   exit
                   ;;
                  "1080p60fps (Full-High-Definition + 60fps)")
                   echo ---------------------------------------------------
                   echo "Bitte füge den Link zu dem Video ein (Strg / Ctrl + Shift + V oder Rechtsklick + Einfügen > Enter)."
                   echo ---------------------------------------------------
                   while read INPUT
                   do
                    echo Wird heruntergeladen...
                    yt-dlp -f 299+bestaudio --merge-output-format mp4 -o "$DOWNDIR%(title)s.%(ext)s" "$INPUT"
                    break
                   done
                   exit
                   ;;
                  "1440p (Quad-High-Definition)")
                   echo ---------------------------------------------------
                   echo "Bitte füge den Link zu dem Video ein (Strg / Ctrl + Shift + V oder Rechtsklick + Einfügen > Enter)."
                   echo ---------------------------------------------------
                   while read INPUT
                   do
                    echo Wird heruntergeladen...
                    yt-dlp -f 400+bestaudio --merge-output-format mp4 -o "$DOWNDIR%(title)s.%(ext)s" "$INPUT"
                    break
                   done
                   exit
                   ;;
                  "1440p60fps (Quad-High-Definition + 60fps)")
                   echo ---------------------------------------------------
                   echo "Bitte füge den Link zu dem Video ein (Strg / Ctrl + Shift + V oder Rechtsklick + Einfügen > Enter)."
                   echo ---------------------------------------------------
                   while read INPUT
                   do
                    echo Wird heruntergeladen...
                    yt-dlp -f 400+bestaudio --merge-output-format mp4 -o "$DOWNDIR%(title)s.%(ext)s" "$INPUT"
                    break
                   done
                   exit
                   ;;
                  "2160p (Ultra-High-Definition 4K)")
                   echo ---------------------------------------------------
                   echo "Bitte füge den Link zu dem Video ein (Strg / Ctrl + Shift + V oder Rechtsklick + Einfügen > Enter)."
                   echo ---------------------------------------------------
                   while read INPUT
                   do
                    echo Wird heruntergeladen...
                    yt-dlp -f 401+bestaudio --merge-output-format mp4 -o "$DOWNDIR%(title)s.%(ext)s" "$INPUT"
                    break
                   done
                   exit
                   ;;
                  "2160p60fps (Ultra-High-Definition 4K + 60fps)")
                   echo ---------------------------------------------------
                   echo "Bitte füge den Link zu dem Video ein (Strg / Ctrl + Shift + V oder Rechtsklick + Einfügen > Enter)."
                   echo ---------------------------------------------------
                   while read INPUT
                   do
                    echo Wird heruntergeladen...
                    yt-dlp -f 401+bestaudio --merge-output-format mp4 -o "$DOWNDIR%(title)s.%(ext)s" "$INPUT"
                    break
                   done
                   exit
                   ;;
                  "Beenden")
                   exit
                   ;;
               *) echo Keine bekannte Option. Bitte wähle eine Zahl zwischen 1 bis 16 aus.;;
               esac
        done
        exit
         ;;
          "ÖRR (Öffentlich Rechtlicher Rundfunk (3sat, ARD, Arte, BR, Funk, ZDF, uvw.))")
             echo ---------------------------------------------------
             echo "Füge den Link zum Video ein. (Strg / Ctrl + Shift + V oder Rechtsklick + Einfügen > Enter). Es wird eine Liste mit den verfuegbaren Formaten angezeigt. Bitte die ID's von den gewuenschten Formaten einfuegen. (Zuerst die Video & dann die Audio ID. Die ID's kann man mit der Maus makieren und dann mit STRG / CTRL + SHIFT + C kopieren und mit STRG / CTRL + V einfügen."
             echo ---------------------------------------------------
             while read INPUT
                   do
                    yt-dlp -F $INPUT
                    echo ---------------------------------------------------
                    echo "Bitte ID1 einfügen (Video)"
                    echo ---------------------------------------------------
                    while read ID1
                   	  do
                   	   echo ---------------------------------------------------
                       echo "Bitte ID2 einfügen (Audio)"
                       echo ---------------------------------------------------
                       	 while read ID2
                   	  	  do
                            echo Wird heruntergeladen...
                       	   yt-dlp -f "$ID1"+"$ID2" --merge-output-format mp4 -o "$DOWNDIR%(title)s.%(ext)s" "$INPUT"
                           break
                         done
                         break
                      done
                      break
                    done
                    exit
             	     ;;
          "Andere Plattform")
              echo ---------------------------------------------------
              echo "Bitte füge den Link zu dem Video ein (Strg / Ctrl + Shift + V oder Rechtsklick + Einfügen > Enter)."
              echo ---------------------------------------------------
              while read INPUT
                do
                 echo Wird heruntergeladen...
                 yt-dlp -f bestvideo+bestaudio --merge-output-format mp4 -o "$DOWNDIR%(title)s.%(ext)s" "$INPUT"
                 break
                done
                exit
                 ;;
          "Über")
              echo ---------------------------------------------------
              echo V-CLI-deo ist ein Terminal Skript und wurde von Monstanner erstellt.
              echo V-CLI-deo gibt es für GNU/Linux und Windows.
              echo Aktuelle Version: 1.3.1 20241126 by Monstanner
              echo Link zu V-CLI-deo: https://codeberg.org/Monstanner/V-CLI-deo/
              echo Fork von YouTube-DL MP4 v.1.3.1 20210919
              echo ---------------------------------------------------
              ;;
          "Beenden")
              exit
              ;;
       *) echo Keine bekannte Option. Bitte wähle eine Zahl zwischen 1 bis 16 aus.
       ;;
  esac
done
exit
