@ECHO off
cls
:start
@ECHO OFF
ECHO ---------------------------------------------------
ECHO V-CLI-deo v.1.3.1 20241126 by Monstanner
ECHO ---------------------------------------------------
ECHO 1. YouTube (Videoplattform von Google LLC.)
ECHO 2. OERR (Oeffentlich Rechtlicher Rundfunk darunter 3sat, ARD, Arte, BR Mediathek, Funk und ZDF)
ECHO 3. Andere Plattform
ECHO 4. Ueber
ECHO 5. yt-dlp aktualisieren
ECHO 6. Beenden

set choice=
set /p choice="Von welcher Plattform soll heruntergeladen werden (Zahl eingeben > Enter)?"
if not '%choice%'=='' set choice=%choice:~0,1%
if '%choice%'=='1' goto 1
if '%choice%'=='2' goto 2
if '%choice%'=='3' goto 19
if '%choice%'=='4' goto 4
if '%choice%'=='5' goto 5
if '%choice%'=='6' goto 6
ECHO "%choice%" ist keine bekannte Option. Bitte eine Zahl zwischen 1 bis 6 oder einen Buchstaben von A bis G eingeben.
ECHO.
goto start
:1
ECHO ---------------------------------------------------
ECHO YouTube Download
ECHO ---------------------------------------------------
ECHO H. 144p (Low-Definition)
ECHO I. 240p (Low-Definition)
ECHO J. 360p (Low-Definition)
ECHO K. 480p (Standard-Definition)
ECHO L. 720p (High-Definition)
ECHO M. 720p60fps (High-Definition + 60fps)
ECHO N. 1080p (Full-High-Definition)
ECHO O. 1080p60fps (Full-High-Definition + 60fps)
ECHO P. 1440p (Quad-High-Definition)
ECHO Q. 1440p60fps (Quad-High-Definition + 60fps)
ECHO R. 2160p (Ultra-High-Definition 4K)
ECHO S. 2160p60fps (Ultra-High-Definition 4K + 60fps)
ECHO G. Beenden

set choice=
set /p choice="In welcher Qualitaet soll das Video heruntergeladen werden (Zahl / Buchstabe eingeben > Enter). Das Video sollte in der Qualitaet verfuegbar sein, anderfalls, wird nichts heruntergeladen."
if not '%choice%'=='' set choice=%choice:~0,1%

if '%choice%'=='H' goto 7
if '%choice%'=='h' goto 7
if '%choice%'=='I' goto 8
if '%choice%'=='i' goto 8
if '%choice%'=='J' goto 9
if '%choice%'=='j' goto 9
if '%choice%'=='K' goto 10
if '%choice%'=='k' goto 10
if '%choice%'=='L' goto 11
if '%choice%'=='l' goto 11
if '%choice%'=='M' goto 12
if '%choice%'=='m' goto 12
if '%choice%'=='N' goto 13
if '%choice%'=='n' goto 13
if '%choice%'=='O' goto 14
if '%choice%'=='o' goto 14
if '%choice%'=='P' goto 15
if '%choice%'=='p' goto 15
if '%choice%'=='Q' goto 16
if '%choice%'=='q' goto 16
if '%choice%'=='R' goto 17
if '%choice%'=='r' goto 17
if '%choice%'=='S' goto 18
if '%choice%'=='s' goto 18
if '%choice%'=='G' goto 6
if '%choice%'=='g' goto 6
ECHO "%choice%" ist keine bekannte Option. Bitte eine Zahl zwischen 1 bis 9 oder einen Buchstaben von A bis U eingeben.
ECHO.
goto start

:2
ECHO ---------------------------------------------------
ECHO OERR Download
ECHO ---------------------------------------------------
:3
@ECHO OFF
SET /P URL="[Fuege den Link zum Video ein. (Rechtsklick > Enter)] "
ECHO.
ECHO "Es wird eine Liste mit den verfuegbaren Formaten angezeigt. Bitte die ID's von den gewuenschten Formaten einfuegen. (Zuerst die Video & dann die Audio ID. Die ID's kann man mit der Maus makieren und dann mit STRG / CTRL + SHIFT + C kopieren und mit ein rechtsklick einfuegen)"
yt-dlp -F %URL%
SET /P ID1="ID 1 eingeben (Video)"
SET /P ID2="ID 2 eingeben (Audio)"
yt-dlp -f %ID1%+%ID2% --merge-output-format mp4 -o "#Bitte das README.md lesen./%%(title)s.%%(ext)s" -i --ignore-config --hls-prefer-native %URL%
ECHO.
PAUSE
EXIT
:7
@ECHO OFF
SET /P URL="[Fuege den Link zum Video ein. (Rechtsklick > Enter)] "
ECHO.
yt-dlp -f 133+bestaudio --merge-output-format mp4 -o "#Bitte das README.md lesen./%%(title)s.%%(ext)s" -i --ignore-config --hls-prefer-native %URL%
ECHO.
PAUSE
EXIT
:8
@ECHO OFF
SET /P URL="[Fuege den Link zum Video ein. (Rechtsklick > Enter)] "
ECHO.
yt-dlp -f 133+bestaudio --merge-output-format mp4 -o "#Bitte das README.md lesen./%%(title)s.%%(ext)s" -i --ignore-config --hls-prefer-native %URL%
ECHO.
PAUSE
EXIT
:9
@ECHO OFF
SET /P URL="[Fuege den Link zum Video ein. (Rechtsklick > Enter)] "
ECHO.
yt-dlp -f 134+bestaudio --merge-output-format mp4 -o "#Bitte das README.md lesen./%%(title)s.%%(ext)s" -i --ignore-config --hls-prefer-native %URL%
ECHO.
PAUSE
EXIT
:10
@ECHO OFF
SET /P URL="[Fuege den Link zum Video ein. (Rechtsklick > Enter)] "
ECHO.
yt-dlp -f 135+bestaudio --merge-output-format mp4 -o "#Bitte das README.md lesen./%%(title)s.%%(ext)s" -i --ignore-config --hls-prefer-native %URL%
ECHO.
PAUSE
EXIT
:11
@ECHO OFF
SET /P URL="[Fuege den Link zum Video ein. (Rechtsklick > Enter)] "
ECHO.
yt-dlp -f 136+bestaudio --merge-output-format mp4 -o "#Bitte das README.md lesen./%%(title)s.%%(ext)s" -i --ignore-config --hls-prefer-native %URL%
ECHO.
PAUSE
EXIT
:12
@ECHO OFF
SET /P URL="[Fuege den Link zum Video ein. (Rechtsklick > Enter)] "
ECHO.
yt-dlp -f 136+bestaudio --merge-output-format mp4 -o "#Bitte das README.md lesen./%%(title)s.%%(ext)s" -i --ignore-config --hls-prefer-native %URL%
ECHO.
PAUSE
EXIT
:13
@ECHO OFF
SET /P URL="[Fuege den Link zum Video ein. (Rechtsklick > Enter)] "
ECHO.
yt-dlp -f 137+bestaudio --merge-output-format mp4 -o "#Bitte das README.md lesen./%%(title)s.%%(ext)s" -i --ignore-config --hls-prefer-native %URL%
ECHO.
PAUSE
EXIT
:14
@ECHO OFF
SET /P URL="[Fuege den Link zum Video ein. (Rechtsklick > Enter)] "
ECHO.
yt-dlp -f 299+bestaudio --merge-output-format mp4 -o "#Bitte das README.md lesen./%%(title)s.%%(ext)s" -i --ignore-config --hls-prefer-native %URL%
ECHO.
PAUSE
EXIT
:15
@ECHO OFF
SET /P URL="[Fuege den Link zum Video ein. (Rechtsklick > Enter)] "
ECHO.
yt-dlp -f 400+bestaudio --merge-output-format mp4 -o "#Bitte das README.md lesen./%%(title)s.%%(ext)s" -i --ignore-config --hls-prefer-native %URL%
ECHO.
PAUSE
EXIT
:16
@ECHO OFF
SET /P URL="[Fuege den Link zum Video ein. (Rechtsklick > Enter)] "
ECHO.
yt-dlp -f 400+bestaudio --merge-output-format mp4 -o "#Bitte das README.md lesen./%%(title)s.%%(ext)s" -i --ignore-config --hls-prefer-native %URL%
ECHO.
PAUSE
EXIT
:17
@ECHO OFF
SET /P URL="[Fuege den Link zum Video ein. (Rechtsklick > Enter)] "
ECHO.
yt-dlp -f 401+bestaudio --merge-output-format mp4 -o "#Bitte das README.md lesen./%%(title)s.%%(ext)s" -i --ignore-config --hls-prefer-native %URL%
ECHO.
PAUSE
EXIT
:18
@ECHO OFF
SET /P URL="[Fuege den Link zum Video ein. (Rechtsklick > Enter)] "
ECHO.
yt-dlp -f 401+bestaudio --merge-output-format mp4 -o "#Bitte das README.md lesen./%%(title)s.%%(ext)s" -i --ignore-config --hls-prefer-native %URL%
ECHO.
PAUSE
EXIT
:19
@ECHO OFF
SET /P URL="[Fuege den Link zum Video ein. (Rechtsklick > Enter)] "
ECHO.
yt-dlp -f bestvideo+bestaudio --merge-output-format mp4 -o "#Bitte das README.md lesen./%%(title)s.%%(ext)s" -i --ignore-config --hls-prefer-native %URL%
ECHO.
PAUSE
EXIT
:4
ECHO ---------------------------------------------------
ECHO V-CLI-deo ist ein Terminal Skript und wurde von Monstanner erstellt.
ECHO V-CLI-deo gibt es fuer GNU/Linux und Windows.
ECHO Aktuelle Version: 1.3.1 20241126 by Monstanner
ECHO Link zu V-CLI-deo: https://codeberg.org/Monstanner/V-CLI-deo/
ECHO Fork von YouTube-DL MP4 v.1.3.1 20210919
ECHO ---------------------------------------------------
goto start
:5
@ECHO OFF
ECHO Soll yt-dlp aktualisiert werden?
SET /P X=(J)a oder (N)ein?
IF /I "%X%"=="J" goto :JA
IF /I "%X%"=="N" goto :NEIN
GOTO ENDE

:JA
ECHO yt-dlp wird aktualisiert...
yt-dlp -U
PAUSE
EXIT

GOTO ENDE

:NEIN
ECHO Beende aktualisierung.

EXIT
:6
goto end
:end
pause
